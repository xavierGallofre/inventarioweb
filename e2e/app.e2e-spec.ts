import { InventarioWebPage } from './app.po';

describe('inventario-web App', function() {
  let page: InventarioWebPage;

  beforeEach(() => {
    page = new InventarioWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
